'use strict';

chrome.runtime.onInstalled.addListener(function (details) {
  console.log('previousVersion', details.previousVersion);
});

chrome.tabs.onUpdated.addListener(function (tabId) {
  chrome.pageAction.show(tabId);
});

console.log('\'Allo \'Allo! Event Page for Page Action');

(function (){
  function genericOnClick(info, tab) {
    window.open("http://union.binbinsoft.com/home/wrap?targeturl=" + encodeURIComponent(info.selectionText));
  }
  var contexts = ["selection", "editable"];
  for (var i = 0; i < contexts.length; i++) {
    var context = contexts[i]; {
      // var title = chrome.i18n.getMessage("context_title_douban");
      var title = "在远景联盟中打开";
      var id = chrome.contextMenus.create({
        "title": title,
        "contexts": [context],
        "onclick": genericOnClick
      });
      console.log("'" + context + "' item:" + id);
    }
  }
})();
