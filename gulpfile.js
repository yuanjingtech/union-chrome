var gulp = require('gulp');
var zip = require('gulp-zip');
var unzip = require('gulp-unzip');
gulp.task('translate', function(){
  // var minimatch = require('minimatch')
  gulp.src("./tmp/archive.zip")
  .pipe(unzip())
  .pipe(gulp.dest('./src/_locales'))
});
gulp.task('default', function () {
  return gulp.src(['./app/**/*.*'])
  .pipe(zip('archive.zip'))
  .pipe(gulp.dest('dist'));
});
